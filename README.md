# Fixture Grabber

## Overview

This basic script pulls the fixtures from the Wycombe Wanderers website
and outputs a CSV with all the home games and kick-off times. I wrote it
to allow me to update my Rotary Club's car parking rota quickly.

## How to set up

The project dependencies are handled with [PDM](https://pdm.fming.dev/latest/)
and it should be sufficient to install PDM then run
`pdm install`
to pull in all the required files and create a venv.

## How to use

All the action happens in `app/grab_fixtures.py`.

Simply check the season slug and URL at the top - you can adjust it to fetch
the games of a different club, past season, etc. if you want. Review the
logic that follows - the whole fixture is outputted if any key data is found
to be missing, which might be the case for pre-season friendlies, it seems.

You can also modify the output CSV should you wish.

To run the script, from the root directory:

`pdm run app/grab_fixtures.py`

Output should be found in the output/ directory (there is an example in there)

## Licence

This project is licensed under the MIT License. Please see `MIT-LICENSE` for details.
