import requests
import csv
from datetime import datetime
from icecream import ic

season = '2023-24'

fixtures_url = f"https://webapi.gc.wycombewanderersfcservices.co.uk/v1/fixtures/opta?clientMatches=true&teamID=t112&seasonSlug={season}&pageSize=1000"

r = requests.get(fixtures_url)

fixtures = r.json()

clean_fixtures = []

# Limiting the data used to clean records of the matches we want

for fixture in fixtures['body']:
	if fixture['venue'] == 'Adams Park': # Only interested in home games
		if 'kickOff' not in fixture: # Some local times seem to be missing - this will flag them
			ic('No kickOff!')
			ic(fixture)
		for team in fixture['teamData']:
			if 'side' in team:
				if team['side'] == 'Away': # We want to know the opponents
					kickOff = datetime. strptime(fixture['kickOff'], '%Y-%m-%d %H:%M:%S')
					kickOffDate = kickOff.strftime('%a, %-d %b') # Set your preferred date format
					kickOffTime = kickOff.strftime('%H%M') #... and your time format
					if kickOffTime == '1230': # based on the stated time, we need our volunteers to arrive earlier
						dutyTime = '1000—1230'
					elif kickOffTime == '1500':
						dutyTime = '1300—1500'
					elif kickOffTime == '1900':
						dutyTime = '1715—1900'
					elif kickOffTime == '1945':
						dutyTime = '1800—1945'
					else:
						dutyTime = 'unknown'
						
					# Build the clean list
					clean_fixtures.append({'team': team['teamName'],
                            				'kickOffDate': kickOffDate,
                            				'kickOffTime': kickOffTime,
                                			'dutyTime': dutyTime,
                                   			'kickOffRaw': fixture['kickOff']})
			else:
				ic("No side!")
				ic(team)

#ic(clean_fixtures) # Print the raw data if you wish to
		
# Build a CSV
f = open('output/fixtures.csv','w')
w = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC)
w.writerow(clean_fixtures[0].keys())
for d in clean_fixtures:
	w.writerow(d.values())
f.close()


